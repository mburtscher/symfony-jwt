<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/foo")
     * @IsGranted("ROLE_USER")
     */
    public function foo()
    {
        return new JsonResponse([ "foo" => "bar" ]);
    }

    /**
     * @Route("/api/bar")
     * @IsGranted("ROLE_ADMIN")
     */
    public function bar()
    {
        return new JsonResponse([ "bar" => "foo" ]);
    }
}
