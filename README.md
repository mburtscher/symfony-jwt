This is a simple application illustrating the use of Symfony 5 with [thephpleague/oauth2-server](https://github.com/thephpleague/oauth2-server) and [trikoder/oauth2-bundle](https://github.com/trikoder/oauth2-bundle) to implement OAuth 2 grants and token based API authentication. It has only implemented the [password grant type](https://oauth2.thephpleague.com/authorization-server/resource-owner-password-credentials-grant/) required by common single page applications. Other OAuth grants are documented [here](https://oauth2.thephpleague.com/authorization-server/which-grant/).

**Please read [this documentation on thephpleague/oauth2-server](https://oauth2.thephpleague.com/) first since it will help you understand the terms used in the code!**

## Notes

- Users are hard-coded as admin:admin and user:user with corresponding ROLE_USER and ROLE_ADMIN roles
- Only the password grant type is enabled
- Public/private key is shipped with Git in `private.key` and `public.key` ([instructions](https://oauth2.thephpleague.com/installation/) how to generate)

## Setup

1. Clone and `composer install`
2. Update database configuration in `.env` and `bin/console doctrine:schema:create`
3. Create a OAuth client (the single page application) `bin/console trikoder:oauth2:create-client`

## Obtaining a token

POST a request to `/token` with these _form parameters_:

**grant_type:** password  
**client_id:** [generated client id, see setup]  
**client_secret:** [generated client secret, see setup]  
**username:** [one of user/admin]  
**password:** [one of user/admin]

## Making a request

Send a request to either `/api/foo` (for ROLE_USER, so user and admin) or `/api/bar` (for ROLE_ADMIN, so only for admin) with a `Authorization: Bearer [obtained access token]`.
